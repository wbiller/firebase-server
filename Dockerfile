FROM node:12-alpine

ENV FIREBASE_SERVER_PORT ${FIREBASE_SERVER_PORT:-5000}
RUN apk add --update --no-cache make gcc g++ python && \
    npm install --production firebase-server && \
    apk del make gcc g++ python
CMD node_modules/.bin/firebase-server -p $FIREBASE_SERVER_PORT -a 0.0.0.0