# Firebase Server
An emulator for Firebase.

## How to use this image

Start the emulator
docker run --name firebase-server -e FIREBASE_SERVER_PORT=5000 -p5000:5000 -d wbiller/firebase-server

### Environment variables

#### `FIREBASE_SERVER_PORT`

With this variable you can set the port to use.